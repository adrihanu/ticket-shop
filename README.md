# Tickets shop

Simple web application for selling tickets.

I created this application as a university project during my master's studies.

Tags: `Ruby On Rails`, `Docker`, `Docker Compose`

## Running

```bash
docker-compose up -d
```