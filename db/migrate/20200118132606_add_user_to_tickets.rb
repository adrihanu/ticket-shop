class AddUserToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :user, :integer
  end
end
