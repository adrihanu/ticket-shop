class Ticket < ApplicationRecord
  validates :name, presence: true, length: { minimum: 6 }
  validates :email_address, presence: true
  validates :price, presence: true
  validate :ticket_price
  belongs_to :event
  belongs_to :user

  def ticket_price
    if price < event.price_low
      errors.add('Cena biletu', 'mnijesza od minimalnej ceny na to wydarzenie')
    end
    if price > event.price_high
      errors.add('Cena biletu', 'większa od maksymalnej ceny na to wydarzenie')
    end
  end
end
